import { createServer } from 'node:http';
import prometheusClient from 'prom-client';
import type { NitroAppPlugin } from 'nitropack';

prometheusClient.register.setContentType(
    prometheusClient.Registry.OPENMETRICS_CONTENT_TYPE,
);

export default <NitroAppPlugin> function (nitroApp: NitroApp) {
  const config = {
    port: parseInt(process.env['NITRO_PROMETHEUS_PORT'] ?? '', 10),
    host: process.env['NITRO_PROMETHEUS_HOST'] ?? '127.0.0.1',
    metrics: {
      collectDefault: process.env['NITRO_PROMETHEUS_METRICS_COLLECT_DEFAULT'] === 'true',
      requestDuration: process.env['NITRO_PROMETHEUS_METRICS_REQUEST_DURATION'] === 'true',
    }
  };
  const metricsInterval = prometheusClient.collectDefaultMetrics(typeof config.metrics.collectDefault === 'object' ? config.metrics.collectDefault : {});

    const httpRequestDurationMicroseconds = new prometheusClient.Histogram({
      name: 'http_request_duration_ms',
      help: 'Duration of HTTP requests in ms',
      labelNames: ['method', 'route', 'code'] as const,
      buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500],
    });

  const s = createServer(async (req, res) => {
    res.writeHead(200, { 'Content-Type': prometheusClient.register.contentType });
    res.end(await prometheusClient.register.metrics());
  });
  s.listen(config.port, config.host);
  nitroApp.hooks.hook("close", async () => {
    s.close();
    clearInterval(metricsInterval);
  });
  if (config.metrics.requestDuration) {
    nitroApp.hooks.hook('request', async ({context}) => {
      context.endPromTimer = httpRequestDurationMicroseconds.startTimer();
    });
    nitroApp.hooks.hook('afterResponse', async ({context, node: {req, res}}) => {
      if (context.endPromTimer) {
        try {
          context.endPromTimer({method: req.method, route: context.matchedRoute?.path ?? req.originalUrl, code: res.statusCode});
        } catch (e) {
          console.error(e);
        }
      }
    });
  }
};
