import { defineNuxtModule, createResolver } from '@nuxt/kit'
import { name, version } from '../package.json'

export default defineNuxtModule({
  meta: {
    name,
    version,
    configKey: 'prometheus',
    compatibility: {
      nuxt: '^3.0.0',
    },
  },
  defaults: {
    port: 9091,
    host: '127.0.0.1',
    metrics: {
      collectDefault: true,
      requestDuration: true,
    }
  },
  setup (options, nuxt) {
    const resolver = createResolver(import.meta.url)
    process.env['NITRO_PROMETHEUS_PORT'] = options.port.toString();
    process.env['NITRO_PROMETHEUS_HOST'] = options.host;
    process.env['NITRO_PROMETHEUS_METRICS_COLLECT_DEFAULT'] = options.metrics.collectDefault.toString();
    process.env['NITRO_PROMETHEUS_METRICS_REQUEST_DURATION'] = options.metrics.requestDuration.toString();

    nuxt.hook('nitro:config', (nitro) => {
      nitro.plugins = nitro.plugins || [];
      nitro.plugins.push(resolver.resolve('runtime/nitro-plugin'));
    })
  }
})
