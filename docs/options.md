## Options

### `port`
Default: `9091`

Port where metrics will be available

### `host`
Default: `127.0.0.1`

Host to bind. Use `0.0.0.0` to be available everywhere, `127.0.0.1` mean "only available on the current host"

### `metrics`

Enable/Disable some metrics

#### `collectDefault`
Default: `true`

Send default metrics about nodejs itself. Pass object to send options to `Prometheus.collectDefaultMetrics`.

#### `requestDuration`
Default: `true`

Send request duration tile with routes.